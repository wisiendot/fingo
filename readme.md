# File storage

## Description
Your task is to create a platform for file storage.

## Requirements
Platform should fullfill these requirements:
* web application
* store files
* create directories
* change file/directory name
* move files across directories
* download a file
* show name, number of files, last modification date for each directory
* show name, size, last modification date for each file
* sort files by name (ascending)

System might also have:
* RESTful API
* an option for sorting files by name, size, modification date

System **does not** have to:
* provide any type of user authentication/registration. Everyone should have access to all functionalities

## What matters for review?
On the scale from 1 to 3, where 3 is the most important:
* System should work without errors nor bugs and meet requirements (3)
* Code should be clear (3)
* Application should have an option for changing file storage location - using correct design pattern (2)
* Frontend (1)

## Other notes
You should be able to make this task in less than 4 hours. **Please do not spend more than 6 hours** on this - just send not finished implementation.

You have to create the system **on your own**. Please don't use ready libraries that does the job. We want to know your coding style. 

You can use pure PHP code or any PHP framework that is convenient for you. In case of using PHP framework, please also provide instructions how to run it.

When you are done, zip whole project directory (optionally including `.git`) and send via email to: `praca@fingo.pl`
