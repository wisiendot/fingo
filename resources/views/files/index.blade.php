@extends('layouts.fingo')
@section('content')
    <h1>Fingo file manager</h1>
    <hr>
    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#upload-modal">Upload</button>
    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#mkdir-modal">Create directory</button>
    <hr>
    @include('files/_files_list')

    @include('files/_upload_modal')
    @include('files/_rename_modal')
    @include('files/_create_directory_modal')
@stop