<table class="table table-condensed">
<thead>
	<tr>
		<th>File name</th>
		<th>Size</th>
		<th>Mime type</th>
		<th>Modified</th>
		<th>Options</th>
	</tr>
</thead>
<tbody>
	@foreach ($assets as $asset)
		<tr>
			<td>
				<a href="{{ url('files/open', [$currentLocation, $asset->getNameEncoded()]) }}" title="Open">
					{{ $asset->getName() }}
				</a>
			</td>
			<td>{{ $asset->getSizeFormatted() }}</td>
			<td>{{ $asset->getMime() }}</td>
			<td>{{ $asset->getModified() }}</td>
			<td>
				@if ($asset->isNotDot())
					<a 
						href="{{ url('files/delete', [$currentLocation, $asset->getNameEncoded()]) }}"
						class="add-confirmation"
						title="Delete"
					>
						<i class="glyphicon glyphicon-remove"></i>
					</a>
					<a 
						href="#"
						class="open-rename-modal"
						data-filename="{{ $asset->getName() }}"
						title="Rename"
					>
						<i class="glyphicon glyphicon-edit"></i>
					</a>
				@endif
			</td>
		</tr>
	@endforeach
</tbody>
</table>