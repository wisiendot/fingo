<div class="modal fade" id="rename-modal" tabindex="-1" role="dialog" aria-labelledby="RenameModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Rename file</h4>
            </div>
            <form action="{{ url('files/rename') }}" method="post">
                <input type="hidden" name="_method" value="PUT">
                <div class="modal-body">
                    <input type="hidden" name="current-location" value="{{ $currentLocation }}">
                    <input type="hidden" name="old-file-name" id="old-file-name">
                    <input type="text" name="new-file-name" id="new-file-name">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Rename" name="submit">
                </div>
            </form>
        </div>
    </div>
</div>