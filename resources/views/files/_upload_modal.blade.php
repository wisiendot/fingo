<div class="modal fade" id="upload-modal" tabindex="-1" role="dialog" aria-labelledby="UploadModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Upload file</h4>
            </div>
            <form action="{{ url('files/upload') }}" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <input type="hidden" name="current-location" value="{{ $currentLocation }}">
                    <label for="file-name">Nazwa pliku</label>
                    <input type="text" name="file-name" id="file-name">
                    <input type="file" name="file-to-upload" id="file-to-upload">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Upload" name="submit">
                </div>
            </form>
        </div>
    </div>
</div>