<div class="modal fade" id="mkdir-modal" tabindex="-1" role="dialog" aria-labelledby="MkdirModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Create directory</h4>
            </div>
            <form action="{{ url('files/mkdir') }}" method="post">
                <input type="hidden" name="_method" value="POST">
                <div class="modal-body">
                    <input type="hidden" name="current-location" value="{{ $currentLocation }}">
                    <input type="text" name="new-directory-name" id="new-directory-name">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Create" name="submit">
                </div>
            </form>
        </div>
    </div>
</div>