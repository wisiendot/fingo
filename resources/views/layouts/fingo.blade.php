<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Fingo file manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="./assets/css/main.css">
    <script src="./assets/js/jquery-2.2.4.min.js"></script>
</head>
<body>
    <div class="container">
        @yield('content')
    </div>
    <script src="./bootstrap/js/bootstrap.js"></script>
    <script src="./assets/js/main.js"></script>
</body>
</html>