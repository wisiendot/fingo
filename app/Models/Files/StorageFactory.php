<?php
namespace App\Models\Files;

class StorageFactory
{
    public static function create($type, $currentLocation = '/')
    {
        $storageClassName = 'App\\Models\\Files\\' . ucfirst($type) . 'Storage';

        return class_exists($storageClassName) ? new $storageClassName($currentLocation) : null;
    }
}
