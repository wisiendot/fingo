<?php
namespace App\Models\Files;

class Asset
{
    const TYPE_DIR = 'dir';
    const TYPE_FILE = 'file';

    protected $type;
    protected $name;
    protected $size = 0;
    protected $mime;
    protected $modified;

    public function __construct($type, $name, $size, $mime, $modified)
    {
        $this->type = $type;
        $this->name = $name;
        $this->size = $size;
        $this->mime = $mime;
        $this->modified = $modified;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getNameEncoded()
    {
        return base64_encode($this->name);
    }

    public function getSize()
    {
        return $this->size;
    }

    public function getSizeFormatted()
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        $power = ($this->size > 0) ? floor(log($this->size, 1024)) : 0;
        return number_format($this->size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
    }

    public function getMime()
    {
        return $this->mime;
    }

    public function getModified()
    {
        return $this->modified;
    }

    public function isNotDot()
    {
        return !in_array($this->name, ['.', '..']);
    }
}
