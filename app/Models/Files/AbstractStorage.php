<?php
namespace App\Models\Files;

use Exception;

abstract class AbstractStorage
{
    protected $homeLocation = '';
    protected $currentLocation = '';

    public function __construct($homeLocation = '/')
    {
        if ($this->isLocationValid($homeLocation)) {
            $this->homeLocation = $homeLocation;
            $this->currentLocation = $homeLocation;
        } else {
            throw new Exception('Such a location does not exist');
        }
    }

    public function getCurrentLocation()
    {
        return $this->currentLocation;
    }

    public function getHomeLocation()
    {
        return $this->homeLocation;
    }

    abstract public function isLocationValid($location, $fileName = false);
    abstract public function isDirectory($location, $fileName);
    abstract public function isFile($location, $fileName);
    abstract public function setNewLocation($location, $fileName);
    abstract public function getAssets();
    abstract public function makeDir($location, $dirName);
    abstract public function rename($location, $oldfileName, $newfileName);
    abstract public function getFile($location, $fileName);
    abstract public function getMime($location, $fileName);
    abstract public function upload($location, $file, $fileName);
}
