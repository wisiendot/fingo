<?php
namespace App\Models\Files;

use App\Models\Files\AbstractStorage;
use App\Models\Files\Asset;

class LocalStorage extends AbstractStorage
{
    public function isLocationValid($location, $fileName = false)
    {
        $location .= $fileName ? DIRECTORY_SEPARATOR . $fileName : '';
        return file_exists($location);
    }

    public function isDirectory($location, $fileName)
    {
        $filePath = $location . DIRECTORY_SEPARATOR . $fileName;
        return is_dir($filePath);
    }
    
    public function isFile($location, $fileName)
    {
        $filePath = $location . DIRECTORY_SEPARATOR . $fileName;
        return is_file($fileName);
    }

    public function setNewLocation($location, $fileName = false)
    {
        $location .= $fileName ? DIRECTORY_SEPARATOR . $fileName : '';
        if (file_exists($location)) {
            $this->currentLocation = $location;
        }
    }
    
    public function getAssets()
    {
        $assets = [];
        $filesList = scandir($this->currentLocation);
        foreach ($filesList as $fileName) {
            $filePath = $this->currentLocation . DIRECTORY_SEPARATOR . $fileName;
            array_push(
                $assets,
                new Asset(
                    is_dir($filePath) ? Asset::TYPE_DIR : Asset::TYPE_FILE,
                    $fileName,
                    $this->getSize($filePath),
                    mime_content_type($filePath),
                    date('d.m.Y H:i:s', filemtime($filePath))
                )
            );
        }
        return $assets;
    }

    public function getFile($location, $fileName)
    {
        $filePath = $location . DIRECTORY_SEPARATOR . $fileName;
        return file_get_contents($filePath);
    }

    public function getMime($location, $fileName)
    {
        $filePath = $location . DIRECTORY_SEPARATOR . $fileName;
        return mime_content_type($filePath);
    }

    public function makeDir($location, $dirName)
    {
        $dirPath = $location . DIRECTORY_SEPARATOR . $dirName;
        return (strrpos($location, $this->homeLocation) !== false) ? mkdir($dirPath) : false;
    }

    public function rename($location, $oldfileName, $newfileName)
    {
        $oldFilePath = $location . DIRECTORY_SEPARATOR . $oldfileName;
        $newFilePath = $location . DIRECTORY_SEPARATOR . $newfileName;
        if (empty($newfileName) || strpbrk($newfileName, "\\/?%*:|\"<>") === true) {
            return false;
        }
        return file_exists($oldFilePath) ? rename($oldFilePath, $newFilePath) : false;
    }

    public function delete($location, $fileName)
    {
        $filePath = $location . DIRECTORY_SEPARATOR . $fileName;
        if (is_dir($filePath)) {
            $this->deleteDirectory($filePath);
        } else {
            unlink($filePath);
        }
        return !file_exists($filePath);
    }

    public function upload($location, $file, $fileName)
    {
        return $file->move($location, $fileName);
    }

    protected function getSize($filePath)
    {
        return is_dir($filePath) ? $this->getDirectorySize($filePath) : filesize($filePath);
    }

    protected function getDirectorySize($directoryPath)
    {
        $filesList = scandir($directoryPath);
        $totalSize = 0;
        foreach ($filesList as $fileName) {
            if (!in_array($fileName, ['.', '..'])) {
                $filePath = $directoryPath . DIRECTORY_SEPARATOR . $fileName;
                $totalSize += is_dir($filePath) ? $this->getDirectorySize($filePath) : filesize($filePath);
            }
        }
        return $totalSize;
    }

    protected function deleteDirectory($location)
    {
        if (is_dir($location)) {
            $objects = scandir($location);
            foreach ($objects as $object) {
                if (!in_array($object, ['.', '..'])) {
                    $filePath = $location . DIRECTORY_SEPARATOR . $object;
                    if (is_dir($filePath)) {
                        $this->deleteDirectory($filePath);
                    } else {
                        unlink($filePath);
                    }
                }
            }
            reset($objects);
            rmdir($location);
        }
    }
}
