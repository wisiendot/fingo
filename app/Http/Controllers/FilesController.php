<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Files\StorageFactory;
use Exception;
use Illuminate\Http\Request;

class FilesController extends Controller
{
    protected $storage = null;

    public function __construct()
    {
        $this->storage = StorageFactory::create('local', storage_path('app'));
        if (is_null($this->storage)) {
            throw new Exception('Indicated storage does not exist');
        }
    }

    public function index($location = false)
    {
        if ($location) {
            $this->storage->setNewLocation($this->decode($location));
        }
        $assets = $this->storage->getAssets();
        $currentLocation = base64_encode($this->storage->getCurrentLocation());

        return view(
            'files.index',
            compact('assets', 'currentLocation')
        );
    }

    public function open($location, $fileName)
    {

        $location = $this->decode($location);
        $fileName = $this->decode($fileName);

        if (($location == $this->storage->getHomeLocation()) && in_array($fileName, ['.', '..'])) {
            return redirect(url('/'));
        }
        if ($this->storage->isDirectory($location, $fileName)) {
            $this->storage->setNewLocation($location, $fileName);
            return redirect('/' . base64_encode($this->storage->getCurrentLocation()));
        } else {
            if ($this->storage->isLocationValid($location, $fileName)) {
                return response()->make(
                    $this->storage->getFile($location, $fileName),
                    200,
                    ['Content-Type' => $this->storage->getMime($location, $fileName)]
                );
            } else {
                throw new Exception('This file does not exist');
            }
        }
    }

    public function rename(Request $request)
    {
        $locationEncoded = $request->get('current-location');
        $location = $this->decode($locationEncoded);
        $oldfileName = $request->get('old-file-name');
        $newfileName = $request->get('new-file-name');
        if ($this->storage->rename($location, $oldfileName, $newfileName)) {
            return redirect('/' . $locationEncoded);
        } else {
            throw new Exception('Cannot rename a file');
        }
    }

    public function mkdir(Request $request)
    {
        $locationEncoded = $request->get('current-location');
        $location = $this->decode($locationEncoded);
        $newDirectoryName = $request->get('new-directory-name');
        if ($this->storage->makeDir($location, $newDirectoryName)) {
            return redirect('/' . $locationEncoded);
        } else {
            throw new Exception('Cannot create a new directory');
        }
    }

    public function delete($location, $fileName)
    {
        $locationEncoded = $location;
        $location = $this->decode($location);
        $fileName = $this->decode($fileName);
        if ($this->storage->delete($location, $fileName)) {
            return redirect('/' . $locationEncoded);
        } else {
            throw new Exception('Cannot delete a file');
        }
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('file-to-upload') && $request->has('file-name')) {
            $locationEncoded = $request->get('current-location');
            $location = $this->decode($locationEncoded);
            $this->storage->upload($location, $request->file('file-to-upload'), $request->get('file-name'));
            return redirect('/' . $locationEncoded);
        } else {
            throw new Exception('Cannot upload a file');
        }
    }

    protected function decode($string)
    {
        return base64_decode(urldecode($string));
    }
}
