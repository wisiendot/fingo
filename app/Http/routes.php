<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$app->get('/', 'FilesController@index');
$app->get('/{location}', 'FilesController@index');
$app->post('files/upload', 'FilesController@upload');
$app->get('files/open/{location}/{filename}', 'FilesController@open');
$app->put('files/rename', 'FilesController@rename');
$app->post('files/mkdir', 'FilesController@mkdir');
$app->get('files/delete/{location}/{filename}', 'FilesController@delete');
