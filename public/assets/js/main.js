$(document).ready(function(){
	$('.add-confirmation').click(function() {
		return confirm('Are you sure?');
	});

	$('.open-rename-modal').click(function() {
		$('#rename-modal').modal('show');
		$('#old-file-name').val($(this).data('filename'));
		$('#new-file-name').val($(this).data('filename'));
	});
});